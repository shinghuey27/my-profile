import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Modal from 'react-modal';


ReactDOM.render(
    <App />,
    document.querySelector('#root'),
    Modal.setAppElement(document.getElementById('root'))

);