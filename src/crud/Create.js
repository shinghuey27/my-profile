import { useState } from "react";
import styles from "./crud.module.scss";

const Create = (props) => {
  const initialFormState = { id: null, name: "", username: "" };
  const [user, setUser] = useState(initialFormState);
  const handleInputChange = (event) => {
    const { name, value } = event.target;

    setUser({ ...user, [name]: value });
  };

  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        if (!user.name?.trim() || !user.username?.trim()) return alert("Please fill in both Name and Username");
        

        props.addUser(user);
        setUser(initialFormState);
      }}
    >
      <div>
        <div className={styles.nameForm}>
          <label className={styles.label}>Name</label>
          <input
            type="text"
            name="name"
            value={user.name}
            onChange={handleInputChange}
            className={styles.input}
            autocomplete="off"
            placeholder="Enter Name"
            required
          />
        </div>
        <div className={styles.usernameForm}>
          <label className={styles.label}>Username</label>
          <input
            type="text"
            name="username"
            value={user.username}
            onChange={handleInputChange}
            className={styles.input}
            autocomplete="off"
            placeholder="Enter Username"
            required
          />
        </div>
      </div>

      <button className={styles.button}>Add</button>
      <button
        onClick={() => props.setAddIsOpen(false)}
        className={styles.button}
      >
        Cancel
      </button>
    </form>
  );
};

export default Create;
