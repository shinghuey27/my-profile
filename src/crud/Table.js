import styles from './crud.module.scss'

const Table = (props) => {
 
  return(
  <table id = {styles.users}>
    <thead>
      <tr>
        <th>Name</th>
        <th>Username</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
    {props.users.length > 0 ? (
        props.users.map((user) => (
          <tr key={user.id}>
            <td>{user.name}</td>
            <td>{user.username}</td>
            <td>

              <button onClick = {()=> props.editRow(user)} className={styles.button}>Edit</button>
              <button onClick = {()=> props.deleteUser(user.id)}className={styles.button}>Delete</button>
            </td>
          </tr>
        ))
      ) : (
        <tr>
          <td colSpan={3}>No users</td>
        </tr>
      )}
    </tbody>
  </table>
  )
      }

export default Table;