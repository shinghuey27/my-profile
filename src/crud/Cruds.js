import { useState } from "react";
import styles from "./crud.module.scss";
import Table from "./Table";
import Create from "./Create";
import Edit from "./Edit";
import Modal from "react-modal";

const Crud = () => {
  const usersData = [
    { id: 1, name: "John", username: "johnhello" },
    { id: 2, name: "Stella", username: "Stellaidol" },
    { id: 3, name: "Emily", username: "Emilybrooke" },
  ];

  const [users, setUsers] = useState(usersData);
  const [edit, setEdit] = useState(false);

  const initialFormState = { id: null, name: "", username: "" };
  const [currentUser, setCurrentUser] = useState(initialFormState);

  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [addIsOpen, setAddIsOpen] = useState(false);

  const click = (event) => {
    setAddIsOpen(true);
  };
  const editRow = (user) => {
    setEdit(true);
    setModalIsOpen(true);
    console.log(modalIsOpen);
    setCurrentUser({ id: user.id, name: user.name, username: user.username });
  };
  const updateUser = (id, updatedUser) => {
    setEdit(false);

    setUsers(users.map((user) => (user.id === id ? updatedUser : user)));
  };
  const addUser = (user) => {
    user.id = users.length + 1;
    setUsers([...users, user]);
  };

  const deleteUser = (id) => {
    setUsers(users.filter((user) => user.id !== id));
  };

  return (
    <div className={styles.container}>
      <h3 className="text-wiki text-center my-2">CRUD App with Hooks</h3>
      <div className={styles.flex}>
        {edit ? (
          <Modal
            isOpen={modalIsOpen}
            shouldCloseOnOverlayClick={false}
            onRequestClose={() => setModalIsOpen(false)}
            style={{
              overlay: {
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: "100vh",
                padding: 0,
                margin: 0,
                backgroundColor: "rgba(0,0,0,0.7)",
                zIndex: 10,
              },
              content: {
                position: "none",
                borderRadius: "20px",
                padding: "20px",
                width: "400px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                border: "none",
              },
            }}
          >
            <div className={styles.flexLarge}>
              <h2 className={styles.editTitle}>Edit user</h2>
              <Edit
                setEdit={setEdit}
                currentUser={currentUser}
                updateUser={updateUser}
              />
            </div>
          </Modal>
        ) : (
          <Modal
            isOpen={addIsOpen}
            shouldCloseOnOverlayClick={false}
            onRequestClose={() => setAddIsOpen(false)}
            style={{
              overlay: {
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: "100vh",
                padding: 0,
                margin: 0,
                backgroundColor: "rgba(0,0,0,0.7)",
                zIndex: 10,
              },
              content: {
                position: "none",
                borderRadius: "20px",
                padding: "20px",
                width: "400px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                border: "none",
                backgroundColor: "#f5f5f5 ",
              },
            }}
          >
            <div className={styles.flexLarge}>
              <h2 className={styles.editTitle}>Add new user</h2>
              <Create addUser={addUser} setAddIsOpen={setAddIsOpen} />
            </div>
          </Modal>
        )}
        <div className={styles.flexLarge}>
          <Table users={users} editRow={editRow} deleteUser={deleteUser} />
          <div className={styles.buttonOverview}>
            <button className={styles.addButton} onClick={click}>
              Add
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Crud;
