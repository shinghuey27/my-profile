import React, { useState } from 'react'
import styles from "./crud.module.scss";


const Edit = (props) => {
    const [user, setUser] = useState(props.currentUser)

    const handleInputChange = (event) => {
        const { name, value } = event.target
    
        setUser({ ...user, [name]: value })
      }


      const click =(event) => {
        props.setEdit(false); 
        // setOpen(false);
      }

      return (
        <form
        onSubmit={(event) => {
          
          event.preventDefault()
          if (!user.name?.trim() || !user.username?.trim()) return alert("Please fill in both Name and Username");
          props.updateUser(user.id, user)
        }}
      >
        <div className ={styles.nameForm}>
        <label className={styles.label}>Name</label>
        <input
          type="text"
          name="name"
          value={user.name}
          onChange={handleInputChange}
          className={styles.input}
          autocomplete="off"
          required
        />
        </div>
        <div className ={styles.usernameForm}>
        <label className={styles.label}>Username</label>
        <input
          type="text"
          name="username"
          value={user.username}
          onChange={handleInputChange}
          className={styles.input}
          autocomplete="off"
          required
        />
        </div>
        <button className={styles.button}>Update</button>
        
  
        <button
          onClick={click}
          className={styles.button}
        >
          Cancel
        </button>
        
      </form>

      )
}

export default Edit;