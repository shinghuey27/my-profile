import React, { useEffect, useState } from "react";
import axios from "axios";
import "../css/thing.scss";

const Wikipedia = () => {
  const [term, setTerm] = useState("programming");
  const [debouncedTerm, setDebouncedTerm] = useState(term);
  const [results, setResults] = useState([]);

  useEffect(() => {
    const timerId = setTimeout(() => {
      setDebouncedTerm(term);
    }, 1000);

    return () => {
      clearTimeout(timerId);
    };
  }, [term]);

  useEffect(() => {
    const search = async () => {
      const { data } = await axios.get("https://en.wikipedia.org/w/api.php", {
        params: {
          action: "query",
          list: "search",
          origin: "*",
          format: "json",
          srsearch: debouncedTerm,
        },
      });
      setResults(data.query.search);
    };

    if (debouncedTerm) {
      search();
    }
  }, [debouncedTerm]);

  const renderedResult = results.map((result) => {
    return (
      <div key={result.pageid} className="item">
        
        <div className="content p-1">
          <h5 className="header text-wiki py-1">{result.title}</h5>
          <div className = "d-flex justify-content-between align-items-center">
          <span 
            className="text-wiki-detail"
            dangerouslySetInnerHTML={{ __html: result.snippet }}
          ></span>{" "}
          <div>
            <a
              className="demoButton"
              href={`https://en.wikipedia.org?curid=${result.pageid}`}
            >
              Go
            </a>
          </div>
          </div>
        </div>
      </div>
    );
  });

  return (
    <div className="ui container pb-5">
      <h3 className = "text-wiki text-center p-2"> Wikipedia Search </h3>
      <div className="ui form">
        <div className="field">
          <h4 className="text-wiki">Enter Search Term</h4>
          <input
            value={term}
            onChange={(e) => setTerm(e.target.value)}
            className="input"
          />
        </div>
      </div>
      <div className="ui called list"> {renderedResult}</div>
    </div>
  );
};

export default Wikipedia;
