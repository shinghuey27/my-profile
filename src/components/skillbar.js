import React from "react";
import { animated, useSpring } from "@react-spring/web";
import "../css/Home.scss";

const Skillbar = (props) => {
  const computedWidth = useSpring({
    width: props.value,
    from: {
      width: "0%",
    },
    config: { duration: 1500 },
  });

  return (
    <div>
      <div>
        <div>
          <div className = "innerSkillsHeader paddingT">
          <div>{props.name}</div>
          <div className="value">{props.value}</div>
          </div>
          <div className="fillContainer ">
            <animated.div
              className="fill "
              style={computedWidth}
            ></animated.div>
          </div>
          <div className = "innerSkillLabel">
            <div className="ui circular labels">
              
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Skillbar;
