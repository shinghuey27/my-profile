import React from "react";
import "../css/AboutMe.scss";
import Timeline from "./Timeline";
import Skillbar from "./skillbar";

const skills = [
  {
    name: "HTML / CSS / SASS",
    value: "70%",
  },
  {
    name: "ReactJS",
    value: "70%",
  },
  {
    name: "Bootstrap",
    value: "50%",
  }
];

const skills2 = [
  {
    name: "Ant Design",
    value: "50%",
  },
  {
    name: "Material UI",
    value: "50%",
  },
  {
    name: "Tailwind",
    value: "30%",
  }
];






const AboutMe = () => {

  return (
    <div>
      <div>
        <div className="pb-2">
          <h1 className="skillHeader">Skills</h1>
          <div className="flex">
            <div className="container1 margin-container-skills">
              <div>
                {skills.map((x, index) => {
                  return <Skillbar name={x.name} value={x.value} key={x.name}/>;
                })}
              </div>
            </div>
            <div className="container1 margin-container-skills">
              {skills2.map((x, index) => {
                return <Skillbar name={x.name} value={x.value} />;
              })}
            </div>
          </div>
        </div>
      </div>

      <Timeline />
      
 
      </div>
  );
};

export default AboutMe;
