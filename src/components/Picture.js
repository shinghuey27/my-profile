import React from "react";
import Typical from "react-typical";
import pp from "../css/me.jpg";
import "../css/Header.css";

const Picture = () => {
  return (
    <div>
      <div className="slide">
        <img src={pp} className="pplogo" alt="logo" />
        <div>
          <h1 className="headerh1 pt-3">Hello, I'm Shing Huey</h1>
          <h2 className="headerh2">
            I {""}
            <Typical
              loop={Infinity}
              wrapper="b"
              steps={[
                "was a Software System Tester.",
                500,
                "am a Front-End Developer now.",
                500,
              ]}
            />
          </h2>
        </div>
      </div>
      <div className="socialmedia">
        <a
          href="mailto: shinghuey27@hotmail.com"
          className="fas fa-envelope"
          target="_blank"
          rel="noreferrer"
        >
          {" "}
        </a>
        <a
          href="https://github.com/shinghuey27"
          className="fab fa-github"
          target="_blank"
          rel="noreferrer"
        >
          {" "}
        </a>

        <a
          href="https://www.linkedin.com/in/shing-huey-wong-851068a7/"
          className="fab fa-linkedin"
          target="_blank"
          rel="noreferrer"
        >
          {" "}
        </a>

        <a
          href="https://wa.link/4a7j9l"
          className="fab fa-whatsapp"
          target="_blank"
          rel="noreferrer"
        >
          {" "}
        </a>
      </div>
    </div>
  );
};

export default Picture;
