import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import "../css/Header.css";

const Header = () => {

  const currentPathname = window.location.pathname;
  const [open, setMenuOpen] = useState(0);

  useEffect(() => {
    switch (currentPathname) {
      case "/":
        return setMenuOpen(0);
      case "/aboutme":
        return setMenuOpen(1);
      case "/thingslearned":
        return setMenuOpen(2);
      case "/ContactMe":
        return setMenuOpen(4);
        case "/season":
        return setMenuOpen(2);
        case "/youtube":
          return setMenuOpen(2);
          case "/translate":
            return setMenuOpen(2);
            case "/wikipedia":
              return setMenuOpen(2);
              case "/crud":
              return setMenuOpen(2);
        default:
          return "/";
    }
  }, [currentPathname]);

  return (
    
    <header className="ui container">
      <div>
        <div className="ui container">
          <div className="ui large secondary inverted pointing menu slide d-Flex-Center">

            <div className = "hlink">
              <Link
                to="/"
                className={`${open === 0 ? "active item" : "item"} h`}
                onClick={() => setMenuOpen(0)}
                
              >
                Home
              </Link>
              <Link
                to="/aboutme"
                className={`${open === 1 ? "active item" : "item"} h`}
                onClick={() => setMenuOpen(1)}
              >
                About Me
              </Link>


              <Link
                to="/thingslearned"
                className={`${
                  open === 2
                    ? "ui dropdown item visible active"
                    : "ui dropdown item"
                }`}
                onClick={() => setMenuOpen(2)}
              >
                Self Learning
              </Link>
            </div>
          </div>
        </div>
      </div>
  
    </header>
    
  );
};

export default Header;
