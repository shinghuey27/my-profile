import React , {useState} from 'react';
import Dropdown from './Dropdown';
import Convert from './Convert';



const options = [
    {
        label : 'Chinese (Simplified)',
        value : 'zh-CN'
    },
    {
        label : 'Chinese (Traditional)',
        value : 'zh-TW'
    },
    {
        label : 'Hindi',
        value : 'hi'
    },
    {
        label : 'Japanese',
        value : 'ja'
    },
    {
        label : 'Korean',
        value : 'ko'
    },
    {
        label : 'Italian',
        value : 'it'
    },
];

const Translate = () => {
    const [language, setLanguage] = useState(options[0]);
    const [text, setText] = useState('');
    
    return (
       
        <div className ="ui container pb-5" >
      <h3 className = "text-wiki text-center p-2"> Google Translate </h3>

            <div className = "ui form">
                <div className = "field">
                    <label className ="text-white"> Enter Text </label>
                    <input value = {text} onChange= {(e) => setText(e.target.value)} />
                </div>
            </div>
          


            <Dropdown 
            label = "Select a Language"
            selected ={language}
            onSelectedChange={setLanguage}
            options = {options} 
            />

            <hr style={{backgroundColor:"white"}}/>
            <h3 className = "ui header text-white">Output</h3>
            <Convert text={text} language = {language} />

        </div>
    )
}


export default Translate;