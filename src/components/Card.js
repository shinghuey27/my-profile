import React, { useEffect } from "react";
import Aos from "aos";
import "aos/dist/aos.css";
import { useSpring, animated } from "react-spring";

const calc = (x, y) => [
  -(y - window.innerHeight / 3) / 50,
  (x - window.innerWidth / 3) / 50,
  1.1,
];
const trans = (x, y, s) =>
  `perspective(600px) rotateX(${x}deg) rotateY(${y}deg) scale(${s})`;

const CardItem = ({
  title,
  description,
  demoLink,
  imgClassName,
  calcFunc,
  cardSide,
}) => {
  const [props, set] = useSpring(() => ({
    xys: [0, 0, 1],
    config: { mass: 1, tension: 280, friction: 60 },
  }));

  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, []);

  return (
    <div className="goC paddingDiv" data-aos="zoom-in" data-aos-duration="500">
      {cardSide === "right" && (
        <div className="selfTitle">
          <h3>{title}</h3>
          <p>{description}</p>

          <a
            href={demoLink}
            className="demoButton"
            target="_blank"
            rel="noreferrer"
          >
            View Demo
          </a>
        </div>
      )}

      <a href={demoLink} target="_blank" rel="noreferrer">
        <animated.div
          className={`card ${imgClassName}`}
          onMouseMove={({ clientX: x, clientY: y }) =>
            set({ xys: calcFunc(x, y) })
          }
          onMouseLeave={() => set({ xys: [0, 0, 1] })}
          style={{ transform: props.xys.to(trans) }}
        />
      </a>
      {cardSide === "left" && (
        <div className="selfTitle">
          <h3>{title}</h3>
          <p>{description}</p>

          <a
            href={demoLink}
            className="demoButton"
            target="_blank"
            rel="noreferrer"
          >
            View Demo
          </a>
        </div>
      )}
    </div>
  );
};

const Card = () => {
  return (
    <div className="backgroundH">
      <CardItem
        title={`Expenses & Grocery App\n(In Progress)`}
        description={`An expenses and grocery app created from scratch using React and integrated with a backend.\n Add budgets or expenses and view detailed information by clicking on the purple button.`}
        demoLink="https://soda-snack-shopping.vercel.app/expenses"
        imgClassName="cardImg7"
        calcFunc={calc}
        cardSide="right"
      />
      <CardItem
        title="Google Map Clone"
        description="A Google Map clone application using React. You can search location by keywords and add to compare the reviews between the location."
        demoLink="https://map-sh.vercel.app/"
        imgClassName="cardImg"
        calcFunc={calc}
        cardSide="left"
      />
      <CardItem
        title="YouTube clone app"
        description="A Youtube clone application using React. You can search videos by keywords and play the video that you choose on this platform."
        demoLink="/youtube"
        imgClassName="cardImg1"
        calcFunc={calc}
        cardSide="right"
      />
      <CardItem
        title="Google Translate"
        description="Google translate using React, You can translate language from dropdown list."
        demoLink="/translate"
        imgClassName="cardImg2"
        calcFunc={calc}
        cardSide="left"
      />
      <CardItem
        title="Wikipedia"
        description="Search something and result will display in a list with a 'Go' button."
        demoLink="/wikipedia"
        imgClassName="cardImg3"
        calcFunc={calc}
        cardSide="right"
      />
      <CardItem
        title="Figma to React"
        description="Completed Figma designed Challenges from devchallenge.io."
        demoLink="/figma"
        imgClassName="cardImg4"
        calcFunc={calc}
        cardSide="left"
      />
      <CardItem
        title="CRUD App with Hooks"
        description="CRUD (create, read, update, delete) app."
        demoLink="/crud"
        imgClassName="cardImg5"
        calcFunc={calc}
        cardSide="right"
      />
      <CardItem
        title="Seasons Change"
        description="Detect user's latitude and longitude to display in summer or winter."
        demoLink="/season"
        imgClassName="cardImg6"
        calcFunc={calc}
        cardSide="left"
      />
    </div>
  );
};

export default Card;
