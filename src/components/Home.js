import React, { useEffect } from "react";
import { useSpring, animated } from "@react-spring/web";
import "../css/Home.scss";

import happy3 from "../css/patrick.gif";

function Home() {
  //slide in
  const [styles, api] = useSpring(() => ({
    from: { x: -1000, opacity: 0 },
  }));
  const currentYear = new Date().getFullYear();
  const age = currentYear - 1998 ;
  useEffect(() => {
    api.start({
      x: 0,
      opacity: 1,
      config: { duration: 1000 },
    });
  }, [api]);

  return (
    <div className="backgroundH">
      <div className="d-flex justify-content-around" style={{ paddingTop: 30 }}>
        <animated.div
          className="slidein big container2"
          style={{
            ...styles,
          }}
        >
          <div className="p4">
            <img src={happy3} alt="happy" />
          </div>

          <div className="vw-80">
            <h1 className="pb-1" style={{ color: "#78cc6d" }}>
              About Me
            </h1>
            <div style={{ float: "left" }}>
              <p>
                <big style={{fontSize:'28px'}}><b>Hi !</b></big> This is my React project created from scratch,
                including multiple small app to play around with.
                {"\n"}
                {"\n"}I am a Front-end Developer with experience in React, currently looking for <strong>new oppurtunity</strong>.
                {"\n"}Feel free to reach out via Email, WhatsApp or LinkedIn if you are
                looking for an extra hand.
              </p>
              <p>
              Check out my <strong>GitHub</strong> via the icon link if you would like to
                take a look into the codes.
                {"\n"}
              </p>
              <p>
                I am a self-taught React JS developer, dedicated to create high-quality and responsive websites.
              </p>
              <p>
              For more information about my background and qualifications, please <a href="resume-2024.pdf" download="resume-2024.pdf"><strong style={{color:'#017698',textDecoration:'underline'}}>download</strong></a> my <strong>resume</strong> from the footer {"\n"} or click 
              {' '}<a href="/aboutme"><strong style={{color:'#017698',textDecoration:'underline'}}>HERE</strong></a> to view my other information.
              </p>
            </div>
          </div>
        </animated.div>
      </div>
      <animated.div
        className="slidein big"
        style={{
          ...styles,
        }}
      >
        <div className="info-list">
          <ul>
            <li>
              <strong>Name: </strong>
              Wong Shing Huey
            </li>
            <li>
              <strong>Age: </strong>
              {age}
            </li>

            <li>
              <strong>Phone: </strong>
              +60107870907
            </li>
          </ul>
        </div>


        <div className="container meDiv">
          <a href="/aboutme" className="meButton">
            Read More
          </a>
        </div>
      </animated.div>
    </div>
  );
}

export default Home;
