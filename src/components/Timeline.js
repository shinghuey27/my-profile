import React from "react";
import TimelineData from "./TimelineData";
import { ReactComponent as WorkIcon } from "../css/work.svg";
import { ReactComponent as SchoolIcon } from "../css/school.svg";

import {
  VerticalTimeline,
  VerticalTimelineElement,
} from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";
import "../css/Timeline.scss";

const Timeline = () => {
  let workIconStyles = { background: "#06D6A0" };
  let schoolIconStyles = { background: "#f9c74f" };

  const sortedTimelineData = TimelineData.sort((a, b) => a.id - b.id);

  return (
    <div>
      <h1 className="text-center paddingT paddingB skillHeader">Timeline</h1>
      <VerticalTimeline>
        {sortedTimelineData.map((e) => {
          let isWorkIcon = e.icon === "work";
          return (
            <VerticalTimelineElement
              key={e.id}
              date={e.date}
              dateClassName="setdate"
              iconStyle={isWorkIcon ? workIconStyles : schoolIconStyles}
              icon={isWorkIcon ? <WorkIcon /> : <SchoolIcon />}
            >
              <h3 style={{fontSize:"1.6rem"}} className="vertical-timeline-element-title">{e.title}</h3>
              <h5 style={{fontSize:"1.1rem"}} className="vertical-timeline-element-subtitle">
                {e.location}
              </h5>
              <ul>
              {e.description.map((sentence, index) => (
                  <li id ="description" className ="split" key={index}>{sentence}</li>
                ))}
              </ul>

            </VerticalTimelineElement>
          );
        })}
      </VerticalTimeline>
    </div>
  );
};

export default Timeline;
