let TimelineData = [
  {
    id: 5,
    title: "Diploma in Business Information System",
    location: "Tunku Abdul Rahman University College",
    description: [
      "Graduated with CGPA 3.07."
    ],
    buttonText: "",
    date: "2016 - 2018",
    icon: "school",
  },
  {
    id: 4,
    title: "BA(Hons) in Enterprise Information System",
    location: "Tunku Abdul Rahman University College",
    description: [
      "Graduated with CGPA 3.34."
    ],
    buttonText: "",
    date: "2018 - 2020",
    icon: "school",
  },
  {
    id: 3,
    title: "Internship of System Tester",
    location: "Izyoo Platform Sdn Bhd",
    description: [
      "6 months internship."
    ],
    buttonText: "",
    date: "Feb 2020 - Aug 2020",
    icon: "work",
  },
  {
    id: 2,
    title: "Software System Tester",
    location: "Izyoo Platform Sdn Bhd",
    description: [
      "Understanding projects scope and BRD.",
      "Create Test Scenarios for project testing.",
      "Black Box and White Box Testing.",
      "Self-learned on Selenium for ease of testing.",
      "Multiple platform testing from multiple user perspectives.",
      "Mathematically calculating and testing on Multi-Level Marketing schemes."
    ],
    buttonText: "",
    date: "Aug 2020 - May 2021",
    icon: "work",
  },
  {
    id: 1,
    title: "Front-end Developer",
    location: "Maybank",
    description: [
      "Experienced in Maybank's Front-end Engineering Team until June 2022.",
      "Successfully transitioned to the delivery team in July 2022, contributing to the team's objectives with adaptability and seamless collaboration.",
      "Proficient in creating reusable components, contributing to code modularity and maintainability.",
      "Experience in using Redux for efficient state management.",
      "Actively supported and guided teammates, fostering a collaborative environment.",
      "Converted multiple modules from Angular to React JS."
    ],
    buttonText: "",
    date: "Oct 2021 - Aug 2023",
    icon: "work",
  },
];

export default TimelineData;
