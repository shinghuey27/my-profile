import React from "react";

const Footer = () => {
  return (
      <footer>
        <div className="left-foot">
          Made with ❤️ by shinghuey
        </div>
        <div className="right-foot">
            <div className="d-resume">
              <a href="resume-2024.pdf" className="cv" download="resume-2024.pdf"> 
                Download Resume
              </a>
            </div>
          </div>
      </footer>
  );
};

export default Footer;
