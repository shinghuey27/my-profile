import React from 'react';


const seasonConfig = {

    summer: {
        text: 'Lets hit the beach!',
        iconName : 'sunny'
    },
    winter: {
        text: 'Burr, it is chilly! ',
        iconName : 'snowy'

    }
};

const getSeason = (lat, month) =>{
    if (month > 2 && month < 9 ){
        return lat > 0 ? 'summer' : 'winter';
    } else {
        return lat < 0 ? 'winter' : 'summer';
    }

}

const SeasonDisplay = (props) => {
    const season = getSeason(props.lat, new Date().getMonth());
    const {text, iconName} = seasonConfig[season] 

    return (
        <div className="season-container">
        <div className= {`season-display ${season}`}>
            <i className={`icon-left  ${iconName} `} />
            <div className = "summer-title">{text}</div>
            <div className = "lat-title">Latitude : {props.lat}</div>
            <div className = "long-title">Longitude : {props.long}</div>
            
        </div>
        </div>
    );
 


}

export default SeasonDisplay;