import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import SeasonDisplay from "./SeasonDisplay";
import Spinner from "./Spinner";
import '../css/thing.scss';

const Season = () => {
  const [lat, setLat] = useState(null);
  const [long, setLong] = useState(null);

  const [errorMessage, setErrorMessage] = useState("");

  // componentDidMount() {
  //   navigator.geolocation.getCurrentPosition(function(position) {
  //     console.log("Latitude is :", position.coords.latitude);
  //     console.log("Longitude is :", position.coords.longitude);
  //   });
  // }

  
  useEffect(() => {
    
    window.navigator.geolocation.getCurrentPosition(
      // we called setState to update to latest location
      (position)  => {
        setLat(position.coords.latitude);
        setLong(position.coords.longitude);
      },

      (err) => setErrorMessage(err.message)
    );
    
  });

  const abc = () =>{
    if (errorMessage && !lat) {
      return <div className = "text-white season-display error">Oppps Error: {errorMessage} </div>;
    }

    if (!errorMessage && lat) {
      return <SeasonDisplay lat={lat} long={long}/>;
    }

    return <Spinner message="Please Accept Location Request" />;
  }

  return <div>
      {abc()}
      </div>;
};

export default Season;
