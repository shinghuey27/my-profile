import React, { useState, useEffect } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Home from "./components/Home";
import AboutMe from "./components/AboutMe";
import Header from "./components/Header";
import Things from "./components/things";
import Contact from "./components/Contact";
import Picture from "./components/Picture";
import Season from "./components/Season";
import Footer from "./components/Footer";
import Translate from "./components/Translate";
import Wikipedia from "./components/Wikipedia";
import Display from "./youtube/Display";
import Crud from "./crud/Cruds";
import Figma from "./components/Figma";
import { motion } from "framer-motion";
import "./css/App.scss";
const App = () => {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 1500);
  }, []);
  return (
    <>
      {loading && (
        <div className="boxContainer">
          {Array.from({ length: 3 }).map((_, index) => (
            <motion.div
              key={index}
              className="box"
              animate={{
                scale: [1, 2, 2, 1, 1],
                rotate: [0, 0, 180, 180, 0],
                borderRadius: ['0%', '0%', '50%', '50%', '0%'],
              }}
              transition={{
                duration: 2,
                ease: 'easeInOut',
                times: [0, 0.2, 0.5, 0.8, 1],
                repeat: Infinity,
                repeatDelay: 1,
              }}
            />
          ))}
        </div>
      )}
      {!loading && (
        <div>
          <BrowserRouter>
            <div className="ui inverted vertical masthead center aligned segment ">
              <Header />
            </div>
            <div className="ui inverted vertical masthead center aligned segment ">
              <Picture />
            </div>
            <div>
              <Route path="/" exact component={Home} />
              <Route path="/aboutme" component={AboutMe} />
              <Route path="/thingslearned" component={Things} />
              <Route path="/ContactMe" component={Contact} />
              <Route path="/season" component={Season} />
              <Route path="/translate" component={Translate} />
              <Route path="/wikipedia" component={Wikipedia} />
              <Route path="/youtube" component={Display} />
              <Route path="/crud" component={Crud} />
              <Route path="/figma" component={Figma} />
            </div>
          </BrowserRouter>

          <Footer />
        </div>
      )}
    </>
  );
};
export default App;
