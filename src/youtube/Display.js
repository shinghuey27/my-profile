import React, { useState, useEffect } from 'react';
import SearchBar from './SearchBar';
import VideoList from './VideoList';
import VideoDetail from './VideoDetail';
import useVideos from './useVideos';

const Display = () => {
    const [selectedVideo, setSelectedVideo] = useState(null);
    const [videos, search] = useVideos('ABCDEFG');


    useEffect (() => {
        setSelectedVideo(videos[0]);
    },[videos]);
   


        return (
            <div className="ui container pb-5">
                <SearchBar onFormSubmit={search} />
                <div className="ui grid">
                    <div className="ui row">
                        <div className="sixteen wide column m-4">
                            <VideoDetail video={selectedVideo} />
                        </div>

                        <div className="sixteen wide column m-4">
                            <VideoList onVideoSelect={(video) => setSelectedVideo(video)} videos={videos} />
                        </div>
                    </div>
                </div>
            </div>
        );
};

export default Display;